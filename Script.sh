#!/bin/bash

printf "\n"
printf "\e[1;77m\e[45m   RabbitMQ Installation Script     \e[0m\n"
printf "\n"
sleep 1

#check for OS

ubuntu=$(grep -i "ubuntu" /etc/os-release | wc -l > 2&>1; echo $?)
rabbitmq=$(which rabbitmq-server > 2&>1; echo $?)

if [[ "$rabbitmq" -eq 0 ]]; then
        echo "RabbitMQ is already installed on the server so please remove the same manually......."
        exit 1
else
    # Installation started..........
    echo "RabbitMQ installation is started..........."
    sleep 1

if [ -f /etc/os-release ] && [[ "$ubuntu" -eq 0 ]]; then
    echo ""
    # Installing ERLANG Dependency
    echo "Dependency ERLANG installation started........."
    sleep 1
    wget https://packages.erlang-solutions.com/erlang-solutions_1.0_all.deb
    sudo dpkg -i erlang-solutions_1.0_all.deb
if [ $? -eq 0 ]; then
    echo ""
    echo "Dependancies installtion started........"
    sleep 1
    sudo apt-get update -y
    sudo apt-get install -y erlang erlang-nox
    echo ""
    else
    echo ""
    echo "Your dependecy installation is failed so please install the same  manually"
    # echo "Manual installation steps https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-rabbitmq"
    exit 1
fi
    echo "RabbitMQ Repository adding to the repos........."
echo 'deb http://www.rabbitmq.com/debian/ testing main' | sudo tee /etc/apt/sources.list.d/rabbitmq.list
wget -O- https://www.rabbitmq.com/rabbitmq-release-signing-key.asc | sudo apt-key add -
if [ $? -eq 0 ]; then
echo ""
    echo "Repository adding successsfully.........."
    sleep 1
    echo ""
    else
    echo ""
    echo "Repository adding failed......."
    # echo "Manual installation steps https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-rabbitmq"
    exit 1
fi

# Dependancies installed succesfully
# Update new repo once maybe you will get error
 sudo apt-get update -y > 2&>1

# RabbitMQ Installation from the added repository
 sudo apt-get install -y rabbitmq-server
if [ $? -eq 0 ]; then
 echo ""
 echo "RabbitMQ Installation successfull we are going to start and enable the services"
 sleep 1
 echo ""
else
echo ""
echo "RabbitMQ installtion failed......."
# echo "Manual installation steps https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-rabbitmq"
exit 1
fi

# Service start section
    sudo systemctl start rabbitmq-server
if [ $? -eq 0 ]; then
    echo ""
    echo "RabbitMQ services started successfully"
    sleep 1
    echo ""
    else
    echo ""
    echo "Service starting failed....... Please check server ports......."
    echo "Manual installation steps https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-rabbitmq"
    exit 1
fi

    # Service status and enable
    status=$(sudo systemctl status rabbitmq-server > 2&>1; echo $?)
    if [[ "status" -eq 0 ]]; then
    echo ""
    echo " RabbitMQ Service enabling............"
    echo ""
    sudo rabbitmq-plugins enable rabbitmq_management
    sleep 1
    echo ""
    else
    echo ""
    echo "Service enabling failed....... Please check server ports......."
    # echo "Manual installation steps https://www.digitalocean.com/community/tutorials/how-to-install-and-manage-rabbitmq"
    exit 1
    fi
fi
fi
